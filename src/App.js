import React from "react";
import { ROUTES } from "./routes";
import Main from "./Main";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import NeedPage, { SubmissionSucess } from "./NeedPage";
import GivePage from "./GivePage";
import "antd/dist/antd.css";
import "./index.scss";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path={ROUTES.MAIN} component={Main} />
        <Route path={ROUTES.NEED} component={NeedPage} />
        <Route path={ROUTES.GIVE} component={GivePage} />
        <Route path={ROUTES.GIVE} component={GivePage} />
        <Route path={ROUTES.SUBMISSION_SUCCESS} component={SubmissionSucess} />
      </Switch>
    </Router>
  );
};

export default App;
