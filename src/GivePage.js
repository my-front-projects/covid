import React, { useState, useEffect } from "react";
import get from "lodash.get";
import axios from "axios";
import PhoneInput from "react-phone-number-input";
import distance from "gps-distance";
import { BASE_URL } from "./constants";
import convert from "convert-units";
import LocationSelection from "./LocationSelection";

const Need = props => {
  const {
    _id: id,
    age,
    gender,
    needCategory,
    needDescription,
    latitude: userLat,
    longitude: userLong,
    phoneNumber,
    location,
    name = "someone",
    fufillNeed
  } = props;
  const getDescription = () => {
    if (!age && !gender) return "";
    let genderAbbreviation = "";
    switch (gender) {
      case "female":
        genderAbbreviation = "F";
      case "male":
        genderAbbreviation = "M";
      default:
        genderAbbreviation = "";
    }
    return `(${age || ""}${genderAbbreviation})`;
  };
  const needCoordiantes = get(location, "coordinates", []);
  const [needLong, needLat] = needCoordiantes;
  const milesAway = convert(distance(userLat, userLong, needLat, needLong))
    .from("km")
    .to("mi")
    .toFixed(1);

  return (
    <div>
      <span>
        {name}
        {getDescription()} is {milesAway} miles away and needs {needCategory}:{" "}
        {needDescription}. Contact at {phoneNumber}
      </span>
      <button onClick={() => fufillNeed(id)}>connect</button>
    </div>
  );
};

const GivePage = props => {
  const [needs, setNeeds] = useState([]);
  const [location, setLocation] = useState();
  const [phoneNumber, setPhoneNumber] = useState();

  useEffect(() => {
    const fetchData = async () => {
      if (location.longitude)
        axios
          .get(`${BASE_URL}/get-needs`, {
            params: location
          })
          .then(response => {
            setNeeds(get(response, "data", []));
          })
          .catch(error => console.error(error));
    };
    if (location) fetchData();
  }, [location]);

  const fufillNeed = id => {
    if (!phoneNumber) {
      alert("please enter your phone number!");
      return;
    }
    axios
      .get(`${BASE_URL}/fufill-need`, {
        params: {
          id,
          phoneNumber
        }
      })
      .then(response => {
        console.log(response);
      })
      .catch(error => console.error(error));
  };

  return (
    <>
      <PhoneInput
        defaultCountry="US"
        value={phoneNumber}
        onChange={setPhoneNumber}
      />
      <LocationSelection setLocation={setLocation} />
      {needs.map(need => (
        <Need key={need._id} {...need} {...location} fufillNeed={fufillNeed} />
      ))}
    </>
  );
};

export default GivePage;
