import React from "react";
import { Link } from "react-router-dom";
import { ROUTES } from "./routes";
import styles from "./Main.module.scss";

const Main = () => {
  return (
    <>
      <Link to={ROUTES.NEED} className={styles["need-button"]}>
        I have a need
      </Link>
      <Link to={ROUTES.GIVE} className={styles["give-button"]}>
        I want to give
      </Link>
    </>
  );
};

export default Main;
