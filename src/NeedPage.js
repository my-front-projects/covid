import React, { useState, useRef, useEffect } from "react";
import get from "lodash.get";
import "react-phone-number-input/style.css";
import PhoneInput from "react-phone-number-input";
import FacebookLoginWithButton from "react-facebook-login";
import styles from "./NeedPage.module.scss";
import axios from "axios";
import { ROUTES } from "./routes";
import { BASE_URL } from "./constants";
import { useHistory } from "react-router-dom";
import { Input, Form, Button, Radio, InputNumber } from "antd";
import LocationSelection from "./LocationSelection";

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 5 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 }
  }
};

const tailLayout = {
  wrapperCol: {
    offset: 8
  }
};

const NeedPage = props => {
  const [phoneNumber, setPhoneNumber] = useState();
  const [location, setLocation] = useState();
  const [user, setUser] = useState();
  const history = useHistory();
  const formRef = useRef(null);
  useEffect(() => {
    console.log(user);
    get(formRef, "current.setFieldsValue") &&
      formRef.current.setFieldsValue({ name: get(user, "name") });
  }, [user, formRef]);

  const facebookLogin = (
    <FacebookLoginWithButton
      appId="212488473197194"
      // autoLoad
      fields="name,picture,gender,email,birthday"
      onClick={() => {}}
      callback={response => setUser(response)}
      icon="fa-facebook"
    />
  );

  const userPicture = get(user, "picture") && (
    <img
      src={user.picture.data.url}
      height={user.picture.height}
      width={user.picture.width}
      alt="avatar"
    />
  );

  const nameInput = (
    <Form.Item label="Name" name="name">
      <Input></Input>
    </Form.Item>
  );

  const ageInput = (
    <Form.Item
      name="age"
      label="Age"
      rules={[{ type: "number", min: 0, max: 130 }]}
    >
      <InputNumber />
    </Form.Item>
  );

  const genderSelect = (
    <Form.Item name="gender" label="Gender">
      <Radio.Group buttonStyle="solid">
        <Radio.Button value="male">Male</Radio.Button>
        <Radio.Button value="female">Female</Radio.Button>
        <Radio.Button value="other">other</Radio.Button>
      </Radio.Group>
    </Form.Item>
  );

  const locationInput = (
    <Form.Item label="Location" name="relativeLocation">
      <LocationSelection setLocation={setLocation} />
    </Form.Item>
  );

  const needDescriptionInput = (
    <Form.Item
      label="Need Description"
      name="needDescription"
      rules={[{ required: true, message: "Please describe your need!" }]}
    >
      <Input></Input>
    </Form.Item>
  );

  const needCategoryInput = (
    <Form.Item
      label="Need Type"
      name="needCategory"
      rules={[{ required: true, message: "Please describe your need!" }]}
    >
      <Radio.Group defaultValue="supplies" buttonStyle="solid">
        <Radio.Button value="social">Social</Radio.Button>
        <Radio.Button value="food">Food</Radio.Button>
        <Radio.Button value="supplies">Supplies</Radio.Button>
        <Radio.Button value="care">Care</Radio.Button>
      </Radio.Group>
    </Form.Item>
  );

  const phoneInput = (
    <Form.Item
      label="Phone"
      name="Phone Number"
      rules={[{ required: true, message: "Please put in your phone number!" }]}
    >
      <PhoneInput
        defaultCountry="US"
        className={styles[`phone-input`]}
        value={phoneNumber}
        onChange={setPhoneNumber}
      />
    </Form.Item>
  );

  const onFinish = values => {
    const need = {
      ...values,
      phoneNumber,
      imageUrl: get(user, "picture.data.url")
    };
    axios
      .post(`${BASE_URL}/add-need`, { location, need })
      .then(function(response) {
        history.push({ pathname: ROUTES.SUBMISSION_SUCCESS });
      })
      .catch(function(error) {
        console.log(error);
      });
    console.log("Success:", values);
  };

  const onFinishFailed = errorInfo => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className={styles[`main`]}>
      <p>
        Login with facebook to add your name and photo automatically to the
        request
      </p>
      {facebookLogin}
      {userPicture}

      <Form
        {...formItemLayout}
        initialValues={{
          name: "Abhi",
          age: 24,
          gender: "male",
          needCategory: "social",
          needDescription: "I need to talk to someone"
        }}
        name="basic"
        ref={formRef}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        {nameInput}

        {ageInput}

        {genderSelect}

        {needCategoryInput}

        {needDescriptionInput}

        {locationInput}

        {phoneInput}

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default NeedPage;

export const SubmissionSucess = () => <h1>Thanks for submitting your need</h1>;
