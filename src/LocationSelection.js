import React, { useState, useEffect, useRef } from "react";
import MapGL, { Marker, NavigationControl } from "react-map-gl";
import get from "lodash.get";

const Pin = props => {
  const ICON = `M20.2,15.7L20.2,15.7c1.1-1.6,1.8-3.6,1.8-5.7c0-5.6-4.5-10-10-10S2,4.5,2,10c0,2,0.6,3.9,1.6,5.4c0,0.1,0.1,0.2,0.2,0.3
  c0,0,0.1,0.1,0.1,0.2c0.2,0.3,0.4,0.6,0.7,0.9c2.6,3.1,7.4,7.6,7.4,7.6s4.8-4.5,7.4-7.5c0.2-0.3,0.5-0.6,0.7-0.9
  C20.1,15.8,20.2,15.8,20.2,15.7z`;

  const pinStyle = {
    fill: "#d00",
    stroke: "none"
  };
  return (
    <svg height={props.size} viewBox="0 0 24 24" style={pinStyle}>
      <path d={ICON} />
    </svg>
  );
};

const LocationSelection = props => {
  const [viewport, setViewport] = useState({
    width: 400,
    height: 400,
    zoom: 8
  });

  const [marker, setMarker] = useState({
    longitude: viewport.longitude,
    latitude: viewport.latitude
  });

  const { setLocation } = props;

  useEffect(() => setLocation(marker), [marker, setLocation]);

  const _onMarkerDragEnd = event => {
    setMarker({
      longitude: event.lngLat[0],
      latitude: event.lngLat[1]
    });
  };

  const setUserLocation = () => {
    navigator.geolocation.getCurrentPosition(position => {
      const { longitude, latitude } = position.coords;
      setViewport({
        ...viewport,
        latitude,
        longitude
      });
      if (longitude !== marker.longitude || latitude !== marker.latitude)
        setMarker({ longitude, latitude });
    });
  };
  useEffect(() => setUserLocation(), []);
  if (!get(marker, "latitude") || !get(marker, "longitude"))
    return <p>Loading...</p>;
  return (
    <>
      {/* <button onClick={setUserLocation}>Reset</button> */}

      <MapGL
        {...viewport}
        onViewportChange={setViewport}
        mapboxApiAccessToken="pk.eyJ1IjoiYWdhcmc1IiwiYSI6ImNrN3g5Nzh1azA5cnEzanM4cGppa3A0Z3AifQ.LqE0Z9LS8F1yeXOKeyPc1A"
      >
        <Marker
          longitude={marker.longitude}
          latitude={marker.latitude}
          offsetTop={-20}
          offsetLeft={-10}
          draggable
          onDragEnd={_onMarkerDragEnd}
        >
          <Pin size={20} />
        </Marker>
        <div
          className="nav"
          style={{
            position: "absolute",
            top: 0,
            left: 0,
            padding: "10px"
          }}
        >
          <NavigationControl
            onViewportChange={viewport => setViewport(viewport)}
          />
        </div>
      </MapGL>
    </>
  );
};

export default LocationSelection;
