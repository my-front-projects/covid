export const ROUTES = {
  MAIN: "/",
  NEED: "/need",
  GIVE: "/give",
  SUBMISSION_SUCCESS: "/submission-success"
};
