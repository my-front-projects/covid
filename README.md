# Important Note

Make sure to have the [server](https://gitlab.com/agarg5/covid-server) running while using this application

## Getting Started

Please follow these instructions to get the code running on your local machine

- Install [git](https://git-scm.com/) for version control on your local computer if you haven't done so already
- Install [Visual Studio](https://visualstudio.microsoft.com/) along with the [React Food Truck](https://marketplace.visualstudio.com/items?itemName=burkeholland.react-food-truck) extensions for front-end code (or use your own preferred editor)
- Copy the URL of this repo and clone it to your local machine.
  \$ git clone https://gitlab.com/agarg5/business-sign-on
- cd inside the cloned repo and run `npm install`
- then run `npm run dev` to open the application in the browser
  Congrats, You now have the code up and running!
- make sure to take a look at the `.sample-env` and ask agarg5 for the credentials. When you get the credentials make a new file at the root of the project called `.env` and put the credentials there. Works in conjunction with the [backend](https://gitlab.com/agarg5/covid-server)

## Styling

Please follow the stying convention of using `scss` modules such as `SignIn.js/SignIn.module.scss` for component specific styles, and `_variables.scss`/ `index.scss` for generic styling.

Please also adhere to [BEM (Bock Element, Modifier) conventions](https://codeburst.io/understanding-css-bem-naming-convention-a8cca116d252?gi=a49834a11a28)

The [react chrome extension](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en) will be very helpful for figuring out what components correspond to what.

This [font-chrome extension](https://chrome.google.com/webstore/detail/whatfont/jabopobgcpjmedljpbcaablpmlmfcogm?hl=en) will be helpful in determining which fonts are which

And the [ruler chrome extension](https://pageruler.li/install.html) would be helpful in seeing spacing

## Git Practices

When developing code, please the feature in a new branch. Name this branch something meaningful and include the number of the task that it is solving (for example `23-fix/hover-on-like-button)

Make a merge request after the first commit and include `WIP` in the title as well as attaching a `WIP` label. Doing this helps track progress. Remember to commit early and often, push frequently, and merge/rebase master frequently into your branch to avoid merge conflicts.

When ready to submit the merge request, remember to test in Chrome, Firefox, Safari, IE as well as on mobile. Include a few screenshots or preferably gifs in the description setion of the merge request. Use a software like Gifox to make gifs. Finally, remove the `WIP` from the title and its label and notify the engineering chanel that your code is ready to review.

Please follow the naming convention from this [git style reference](https://github.com/agis/git-style-guide)

## What to get started on

get the code running locally on your computer and play around with it. When you feel like you have a good sense of it, take a look at the issues list at the [issues list](https://gitlab.com/agarg5/business-sign-on/issues). Find one you would like to work on. Assign the issue to yourself and follow the git practices mentioned in this document to work on that issue. Put a completion ETA in the comments of the issue

As you discover other bugs, feature suggestions, please add them to this issue list along with:

- a detailed description,
- gifs + images showing the solution
- the testing plan as well as passing tests if applicable
- challenges encountered
- any code debt created
- the approach taken to solving the problem

## Code Best Practices

- Don't use magic numbers, use const variables defined at top of files instead. const variables should have const prefix and variable names should be in snake case in all caps
- React: make skinny render methods (return statements for functional components). Do this by: 1) make components modular, 2) defining small functional compnents in the same file large components are in if the small components are not being used elsewhere, and 3) seperating small UI blocks into variables defined on class components.
- Don't use abbreviations. Don't use `EvList.js` instead use `Event.js` for example. Abbreviations cause confusion and overhead
- React: Use functional components instead of class components. Use [React Hooks](https://reactjs.org/docs/hooks-overview.html) instead of lifecycle methods and `this.state`. Functional components are more concise, lightweight, and easier to maintain
- Don't use the presentional/functional component paradigm. Insead use functional components. React [this article](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0) for why
